
// msgs
const MSGS = {
  "ZERO_RESULTS" : "検索結果が０件でした。"
}


var app = angular.module("mapApp", []);

app.filter("kCal", function(){
  return function(x){
    const RATE = 0.055;
    return (x * RATE).toFixed(2) + "kCal";
  }
});

// googleMap functions
app.service("map", function(){
  this.calcRoute = function($q, directionDisplay, start, end, middle) {

    var deferred = $q.defer();

    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
  
    var request = {
      origin: start,
      destination: end,
      waypoints: middle,
      travelMode: google.maps.TravelMode.WALKING,
      optimizeWaypoints: true,
      provideRouteAlternatives: false,
      avoidTolls: false,
      region: "jp"
    }
  
    directionsService.route(request, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        document.directionsDisplay.setDirections(result);
        deferred.resolve(result.routes[0]);
      } else {
        deferred.reject(status);
      }
    });
    return deferred.promise;
  }
  
})

app.controller("mapCtrl", function($scope, $q, map){

  // init
  $scope.places = [];
  
  $scope.writeMap = function(isAddPlace){
  
    // 軽めのバリデーション
    $scope.fromPlace = $scope.fromPlace.trim();
    $scope.toPlace = $scope.toPlace.trim();
  
    // $scope.placeはgooglemapの処理成功後にpush
    var tmpPlaces = [];
    if($scope.places.length <= 0){tmpPlaces.push($scope.fromPlace);};
    if($scope.places.length > 0){tmpPlaces = $scope.places;};
    if(isAddPlace){tmpPlaces.push($scope.toPlace);};

    var promise = map.calcRoute($q,
                                document.directionsDisplay,
                                tmpPlaces[0],
                                tmpPlaces[tmpPlaces.length -1],
                                $scope.getMiddlePlaces());
    
    promise.then(function(route){
      $scope.discriptions = $scope.getDiscriptions(route);
      $scope.places = tmpPlaces;
      $scope.toPlace = "";
    }, function(status){
      $scope.places.pop($scope.places.length-1);
      if(status in MSGS){
        alert(MSGS[status]);
      } else {
        alert(status);
      }
    });
  }

  $scope.deletePlaceAndRewriteMap = function(idx){
    if($scope.places.length > 2 && idx > 0){
      $scope.places.splice(idx, 1);
    }
    $scope.writeMap(false);
  }

  $scope.isNotEmptyArr = function(arr){
    return (arr && arr instanceof Array && arr.length > 0);
  }

  $scope.getMiddlePlaces = function(){
    if($scope.isNotEmptyArr($scope.places) && $scope.places.length > 2){
      var wayPoints = [];
      $scope.places.slice(1,$scope.places.length-1).forEach(function(val, index, arr){
        wayPoints.push({location: val});
      })
      return wayPoints;
    } else {
      return [];
    }
  }

  $scope.getDiscriptions = function(route){
    var discriptions = [];
    var legs = route.legs;
    for(var i=0; i<legs.length; i++){
      discriptions.push(
        { distance: legs[i].distance.text,
          distanceVal: legs[i].distance.value,  // filter:kCalに使用
          duration: legs[i].duration.text }
      )
    }
    return discriptions;
  }

})

// googleMap initialize
function initMap() {

  document.directionsDisplay = new google.maps.DirectionsRenderer();
  var mapOptions = {
    zoom:7
  }
  document.directionsDisplay.setMap(new google.maps.Map(document.getElementById("map"), mapOptions));
  document.directionsDisplay.setOptions(
  {
    suppressMarkers: false,
    markerOptions: {
      animation: google.maps.Animation.DROP
    },
    polylineOptions: {
                strokeWeight: 1,
                strokeOpacity: 1,
                strokeColor:  'red' 
    }
  })
}

